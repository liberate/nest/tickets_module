module Support
  class Engine < ::Rails::Engine
    include Nest::UserEngine
    engine_name 'support'
    isolate_namespace Support
  end
end

module Admin
  module Support
    class Engine < ::Rails::Engine
      include Nest::AdminEngine
      engine_name 'admin_support'
      isolate_namespace Admin::Support
    end
  end
end