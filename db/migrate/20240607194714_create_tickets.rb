class CreateTickets < ActiveRecord::Migration[7.1]
  def change
    create_table "support_tickets", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "user_id"
      t.integer "admin_id"
      t.integer "state"
      t.string  "code"
      t.string  "subject"
      t.boolean "is_public"
      t.text    "note"
      t.timestamps
      t.index "code"
    end

    create_table "support_messages", if_not_exists: true, charset: "utf8mb4", collation: "utf8mb4_general_ci" do |t|
      t.integer "ticket_id"
      t.integer "user_id"
      t.text "content"
      t.timestamps
    end

    create_join_table :support_tickets, :users
  end
end
