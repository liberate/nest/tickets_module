Support::Engine.routes.draw do
  resources :tickets
  resources :messages do
    get 'confirm', on: :member
  end
end
