Gem::Specification.new do |spec|
  spec.name        = "tickets_module"
  spec.version     = "0.1.0"
  spec.authors     = ["Nest Developers"]
  spec.summary     = "Help tickets for Nest apps"
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{app,config,db,lib}/**/*", "README.md"]
  end
  spec.add_dependency "rails", ">= 7.1.3.4"
  spec.add_dependency "validates_email_format_of", "~> 1.8"
end
