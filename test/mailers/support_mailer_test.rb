require_relative '../test_helper'

class SupportMailerTest < ActionMailer::TestCase

  def test_message_created
    user = User.create(email: 'person@example.org')
    ticket = Support::Ticket.create!(subject: 'help!', user: user)
    email = Support::Mailer.with(user: user, ticket: ticket).ticket_updated
    assert_emails 1 do
      email.deliver_now
    end

    assert_equal [user.email], email.to

    email_body = email.parts.detect {|part| part.mime_type == "text/plain"}&.body.to_s
    assert email_body.present?

    param = email_body.match(/http:\/\/localhost:?(\d+)?\/link\/([A-Za-z0-9_-]+)/)&.to_a&.fetch(2)
    hsh = SecretUrl::decode(param)
    assert_equal "/support/tickets/#{ticket.code}", hsh[:path]
    assert_equal "tickets", hsh[:auth]
    assert_equal user, hsh[:user]
  end

end
