require_relative '../test_helper'

class TicketTest < ActiveSupport::TestCase

  def create_ticket
    user = User.create!(email: 'ticket_user@example.org')
    Support::Ticket.create!(subject: 'test', user: user)
  end

  def test_create
    ticket = create_ticket
    assert ticket.persisted?
  end

  def test_states
    ticket = create_ticket
    assert ticket.state_new?
    ticket.state_answered!
    ticket.reload
    assert ticket.state_answered?
  end

  def test_user_association
    ticket = create_ticket
    assert_equal 1, ticket.users.count

    user1 = User.first
    user2 = User.second
    user3 = User.third
    assert user1
    assert user2
    assert user3

    ticket.users << user1
    ticket.users << user2
    assert_equal 3, ticket.users.count

    assert ticket.is_user?(user1)
    assert ticket.is_user?(user2)
    assert !ticket.is_user?(user3)
  end

  def test_auto_user_association
    ticket = create_ticket
    assert_equal 1, ticket.users.count
    ticket.admin = User.create(email: 'admin@example.org')
    ticket.save
    assert_equal 2, ticket.users.count
  end

  def test_messages
    ticket = create_ticket

    user1 = User.first
    ticket.messages.create!(content: 'hi', user: user1)

    assert_equal 1, ticket.messages.count
  end

  def test_code
    ticket = create_ticket
    assert ticket.code
    assert ticket.code =~ /^[0-9]+$/
  end
end