# Configure Rails Environment
ENV["RAILS_ENV"] = "test"

require_relative "../../../config/environment"
ActiveRecord::Migrator.migrations_paths << File.expand_path("../db/migrate", __dir__)
require "rails/test_help"
