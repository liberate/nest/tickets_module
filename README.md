# Nest Help Tickets Module

Help tickets and user support for Nest applications.

## License

Copyright (c) 2024 Nest Developers

GNU Affero General Public License, Version 3 or later. https://www.gnu.org/licenses/agpl-3.0.en.html
