module Support
  module TicketHelper
    def ticket_state(ticket, admin: false)
      color = if ticket.state_new?
        'success'
      elsif ticket.state_resolved? || ticket.state_rejected?
        'dark'
      else
        'primary'
      end
      content_tag(:span, class: "badge text-bg-#{color}") do
        if admin
          ticket.state.sub('_', ' ')
        else
          ticket.state_for_user.sub('_', ' ')
        end
      end
    end

  end
end
