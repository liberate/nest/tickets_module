class Admin::Support::TicketsController < AdminApp::ApplicationController
  helper Support::TicketHelper
  before_action :get_ticket, only: [:show, :destroy, :update]

  def index
    @tickets = Support::Ticket.
      order(updated_at: :desc).
      includes(:users).
      page(params[:page])
    if params[:view]
      @tickets = @tickets.where(state: params[:view])
    end
    if params[:filter]
      @tickets = @tickets.left_outer_joins(:users).where('users.id = ?', current_user.id)
    end
  end

  def show
    @new_message = Support::Message.new
  end

  def update
    @ticket = Support::UpdateTicketForm.new(ticket: @ticket, params: params)
    if @ticket.save
      redirect_to ticket_url(@ticket), **@ticket.flash
    else
      render :show
    end
  end

  def new
    if Rails.env.development?
      @ticket = Support::NewTicketForm.new(email: params[:email], subject: Faker::Lorem.sentence, message: Faker::Lorem.paragraph)
    else
      @ticket = Support::NewTicketForm.new(email: params[:email])
    end
  end

  def create
    @ticket = Support::NewTicketForm.new(ticket_params.merge(admin: current_user))
    if @ticket.save
      redirect_to ticket_url(@ticket), notice: "Ticket was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def destroy
    @ticket.destroy
    redirect_to tickets_url
  end

  private

  def ticket_params
    params.require(:ticket).permit(:subject, :message, :email)
  end

  def get_ticket
    @ticket = Support::Ticket.from_param(params[:id])
  end
end