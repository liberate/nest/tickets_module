
class Admin::Support::MessagesController < AdminApp::ApplicationController
  layout 'modal'

  before_action :get_message, only: [:edit, :update, :confirm, :destroy]

  def edit
    render 'support/messages/edit'
  end

  def update
    if @message.update(params.require(:message).permit(:content))
      redirect_to ticket_url(@message.ticket)
    else
      render 'support/messages/edit'
    end
  end

  def create
    @ticket = Support::Ticket.find(params[:ticket_id])
    @new_message = @ticket.messages.build(content: params[:message][:content], user: current_user)
    if @new_message.save
      @ticket.send_notification(@new_message)
      redirect_to ticket_url(@ticket), notice: "Message posted."
    else
      render 'support/messages/new'
    end
  end

  def confirm
    render 'support/messages/confirm'
  end

  def destroy
    @message.destroy
    redirect_to ticket_url(@message.ticket), notice: "Message deleted."
  end

  private

  def get_message
    @message = Support::Message.find(params[:id])
  end
end
