module Support
  class MessagesController < UserApp::ApplicationController
    layout 'modal'

    require_authentication allowed_user_scope: :tickets
    before_action :get_message, only: [:edit, :update, :confirm, :destroy]

    def edit
    end

    def update
      if @message.update(params.require(:message).permit(:content))
        redirect_to ticket_url(@message.ticket)
      else
        render 'edit'
      end
    end

    def create
      @ticket = Ticket.find(params[:ticket_id])
      permit! { current_user.admin? || @ticket.is_user?(current_user) }

      @new_message = @ticket.messages.build(content: params[:message][:content], user: current_user)
      if @new_message.save
        send_notification(@ticket)
        redirect_to ticket_url(@ticket), notice: "Message posted."
      else
        render 'new'
      end
    end

    def confirm
    end

    def destroy
      @message.destroy
      redirect_to ticket_url(@message.ticket), notice: "Message deleted."
    end

    private

    def get_message
      @message = Message.find(params[:id])
      permit! { current_user.admin? || @message.user == current_user }
    end

    def send_notification(ticket)
      if current_user.admin?
        ticket.requesters.each do |user|
          email = Support::Mailer.with(user: user, ticket: ticket).ticket_updated
          email.deliver_now
        end
      end
    end

  end
end
