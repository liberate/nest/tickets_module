module Support
  class TicketsController < UserApp::ApplicationController
    helper Support::TicketHelper

    require_authentication allowed_user_scope: :tickets
    permit :index, :new

    before_action :get_ticket, only: [:show, :destroy, :update]

    def index
      if current_user.admin?
        @tickets = Support::Ticket.
          order(updated_at: :desc).
          includes(:users).
          page(params[:page])
        if params[:view]
          @tickets = @tickets.where(state: params[:view])
        end
        if params[:filter]
          @tickets = @tickets.left_outer_joins(:users).where('users.id = ?', current_user.id)
        end
      else
        @tickets = Support::Ticket.
          order(updated_at: :desc).
          left_outer_joins(:users).
          where('users.id = ?', current_user.id).
          page(params[:page])
      end
    end

    def show
      @new_message = Support::Message.new
    end

    def update
      @ticket = Support::UpdateTicketForm.new(ticket: @ticket, params: params)
      if @ticket.save
        redirect_to ticket_url(@ticket)
      else
        render :show
      end
    end

    def new
      if Rails.env.development?
        @ticket = NewTicketForm.new(subject: Faker::Lorem.sentence, message: Faker::Lorem.paragraph)
      else
        @ticket = NewTicketForm.new
      end
    end

    def create
      permit! {true} # you can always create your own tickets
      @ticket = NewTicketForm.new(ticket_params.merge(user: current_user))
      if @ticket.save
        redirect_to ticket_url(@ticket), notice: "Ticket was successfully created."
      else
        render :new, status: :unprocessable_entity
      end
    end

    def destroy
    end

    private

    def ticket_params
      params.require(:ticket).permit(:subject, :message, :email)
    end

    #
    # allow viewing an order unauthenticated if and only if we
    # are using the encrypted URL
    #
    def get_ticket
      if current_user
        @ticket = Support::Ticket.from_param(params[:id])
        permit! { current_user.admin? || @ticket.is_user?(current_user) }
      else
        @ticket, user, expire = Support::Ticket.parse_encrypted_code(params[:id])
        permit! { @ticket.is_user?(user) }
        scoped_log_in(user, :tickets)
      end
    end
  end
end
