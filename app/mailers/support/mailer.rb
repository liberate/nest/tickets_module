module Support
  class Mailer < Nest::Mailer
    MESSAGE=\
%[Your support ticket has been updated. You can see the message here:

{{ ticket_url }}
]

    before_action {
      @user = params[:user]
      @ticket = params[:ticket]
    }

    def ticket_updated
      encoded = SecretUrl::encode(
        user: @user,
        auth: "tickets",
        path: support.ticket_path(@ticket),
        expires: 4.weeks.from_now
      )
      send_email(
        template: message_template,
        vars: {
          subject: "Ticket Updated (#{@ticket.subject})",
          to: @user.email,
          ticket_url: main_app.link_url(id: encoded)
        }
      )
    end

    def message_template
      MessageTemplate.new(
        body_plain: MESSAGE
      )
    end
  end
end