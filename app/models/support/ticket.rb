#
# attributes:
# string   subject
# boolean  is_public
# text     note
#
module Support
  class Ticket < ApplicationRecord
    self.table_name = 'support_tickets'

    has_code length: 6, type: :numeric

    belongs_to :admin, class_name: "User", optional: true
    belongs_to :user
    has_many :messages, -> { order("created_at") }, dependent: :destroy
    has_and_belongs_to_many :users, foreign_key: :support_ticket_id

    enum :state, [:new, :answered, :replied, :resolved, :rejected], default: :new, prefix: true

    validates :subject, presence: true, length: { maximum: 200 }

    scope :open,   -> { where(state: ['new', 'answered', 'replied']) }
    scope :closed, -> { where(state: ['resolved', 'rejected']) }

    after_save :update_users

    #
    # normal users don't see the full state
    #

    USER_STATES = {
      "new"       => "new",
      "answered"  => "in_progress",
      "replied"   => "in_progress",
      "resolved"  => "closed",
      "rejected"  => "closed"
    }

    def state_for_user
      USER_STATES[state]
    end

    def is_user?(user)
      users.exists?(user.id)
    end

    def open?
      state_new? || state_answered? || state_replied?
    end

    def closed?
      !open?
    end

    # all the non-admin participants of this ticket
    def requesters
      @requesters ||= users.select {|u| !u.admin?}
    end

    # all the admin participants of this ticket
    def admins
      @admins ||= users.select {|u| u.admin?}
    end

    # updates ticket users that something has changed about the ticket
    def send_notification(message)
      if message.user.admin?
        self.requesters.each do |user|
          email = Support::Mailer.with(user: user, ticket: self).ticket_updated
          email.deliver_now
        end
      end
    end

    private

    def update_users
      if admin_previously_changed?
        self.users << admin if admin && !self.users.exists?(admin_id)
      end
      if user_previously_changed?
        self.users << user if user && !self.users.exists?(user_id)
      end
    end
  end
end
