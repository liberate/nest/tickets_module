module Support
  class Message < ApplicationRecord
    self.table_name = 'support_messages'

    belongs_to :user
    belongs_to :ticket, inverse_of: :messages

    validates :content, presence: true

    after_create :update_ticket

    private

    def update_ticket
      update = {}
      if user.admin? && !ticket.state_rejected? && !ticket.state_answered?
        if ticket.messages.count > 1
          update[:state] = :answered
        end
      elsif !ticket.state_new?
        update[:state] = :replied
      end
      if user.admin? && ticket.admin != user
        update[:admin] = user
      end
      if update.any?
        ticket.update(update)
      end
    end

  end
end
