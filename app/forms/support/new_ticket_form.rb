module Support
  class NewTicketForm
    include ActiveModel::Model

    delegate :model_name, to: :@ticket
    delegate :to_param, to: :@ticket

    attr_accessor :subject, :message, :email, :user, :admin

    validates :subject, presence: true
    validates :message, presence: true
    validate :validate_email
    validate :user

    def initialize(*args)
      super
      @ticket = Support::Ticket.new
    end

    def save
      return false unless valid?
      if admin
        author = admin
        self.user ||= Member.find_by_email(email)&.user || User.ensure_user(email)
      else
        author = user
      end
      @ticket.subject = subject
      @ticket.user = user
      @ticket.admin = admin
      @message_record = @ticket.messages.build(content: @message, user: author)
      if @message_record.invalid?
        @message_record.errors.errors.each do |error|
          errors.add(:message, :skip_attribute, message: error.full_message)
        end
      end
      errors.merge!(@ticket.errors) if @ticket.invalid?
      return false if errors.any?
      @ticket.save
    end

    def validate_email
      if email.present?
        self.email = email.downcase
        options = {check_mx: Conf.perform_email_domain_check}
        if ValidatesEmailFormatOf::validate_email_format(email, options) != nil
          errors.add(:email, :invalid)
        end
      elsif admin
        errors.add(:email, :required)
      end
    end
  end
end

