module Support
  class UpdateTicketForm
    include ActiveModel::Model

    delegate :model_name, to: :@ticket
    delegate :to_param, to: :@ticket

    attr_accessor :ticket
    attr_accessor :flash

    def initialize(ticket:, params:)
      @flash = {}
      @ticket = ticket
      if params["update-state"]
        if params["state-closed"] || params["state-resolved"]
          @flash[:notice] = 'Ticket closed.' unless @ticket.closed?
          @ticket.state = :resolved
        elsif params["state-new"]
          @flash[:notice] = 'Ticket opened.' if @ticket.closed?
          @ticket.state = :new
        elsif params["state-progress"] || params["state-replied"]
          @flash[:notice] = 'Ticket opened.' if @ticket.closed?
          @ticket.state = :replied
        elsif params["state-answered"]
          @flash[:notice] = 'Ticket opened.' if @ticket.closed?
          @ticket.state = :answered
        elsif params["state-rejected"]
          @flash[:notice] = 'Ticket closed.' unless @ticket.closed?
          @ticket.state = :rejected
        end
      end
    end

    def save
      @ticket.save
    end
  end
end

